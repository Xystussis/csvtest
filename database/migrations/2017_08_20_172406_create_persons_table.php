<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('people', function (Blueprint $table) {
            $table->increments('id');
            $table->string('legacy_id');
            $table->text('name');
            $table->text('phone')->default('')->nullable();
            $table->integer('age')->default(0)->nullable();
            $table->string('street')->nullable();
            $table->string('zipcode')->nullable();
            $table->string('province')->nullable();
            $table->string('city')->nullable();
            $table->string('country')->nullable();
            $table->json('interests')->nullable();
            $table->string("emailaddress")->nullable();
            $table->string('homepage')->nullable();
            $table->string('creditcard')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('people');
    }
}
