### Some notes ####

There are many ways to solve this puzzle. In general I feel that importing data in to a DB to be processed later is the better/more efficient way.
As such the models are capable of storing the info (category DOES do this, however beyond simply importing in this manner, I have not used it.)
as for Person, this has the ability to store to DB, naturally, but since the expected output was simply CSV, I have ignored the Person save,
and simply used it under the JsonSerializable interface to be written directly to CSV.

### How do I get set up? ###

clone the repo into a directory on your box
composer install
make your .env file (setting up the database, please make sure the database exists on localhost)
php artisan migrate
copy the xml file to the directory
php artisan data:import --file=directory/contactlist.xml


