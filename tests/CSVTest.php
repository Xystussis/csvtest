<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class CSVTest extends TestCase
{
    /**
     * If We give the CSV Writer a bogus file it returns false
     * @test
     * @return void
     */
    public function nonExistantFileReturnsFalse()
    {

        $c = new \App\CSVWriter();


        $this->assertFalse($c->appendToCSV('dsfsefsefes',new \App\Person()));
    }



    /**
     * If we give a good file the CSV can write
     * @test
     */
    public function goodFileReturnsTrue()
    {
        $c = new \App\CSVWriter();

        $this->assertTrue($c->writeArrayToCsv('storage/testCsv.csv',['dsfsdf']));

    }

}
