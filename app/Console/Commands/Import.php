<?php

namespace App\Console\Commands;

use App\CSVWriter;
use App\XmlImporter;
use Illuminate\Console\Command;

class Import extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'data:import  {--F|file=}';



    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import data from a specified file';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ini_set('memory_limit','32M');
        $filename = $this->option('file');

        //Is the file parameter available?
        if (empty($filename))
        {
            $this->info("Usage : data:import --file=filename");
            return;
        }

        //Does the file exist?
        if (!file_exists($filename))
        {
            $this->error("File not found");
        }

        $parser = new XmlImporter();
        $this->info('beginning import');
        $parser->parseFile($filename);
        $this->info('complete');

    }







}
