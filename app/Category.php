<?php
/**
 * Created by PhpStorm.
 * User: Pierre van Horick
 * Date: 2017/08/20
 * Time: 11:19 AM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    protected $guarded = [];


    public function setLegacyId($id="")
    {
        $this->legacy_id = $id;
        return $this;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function setDescription($description)
    {
        $this->description = trim($description);
        return $this;
    }

    public function __toString()
    {
        return "ID:".$this->legacy_id.PHP_EOL."Name: ".$this->name.PHP_EOL."Description: ".$this->description.PHP_EOL;
    }

}