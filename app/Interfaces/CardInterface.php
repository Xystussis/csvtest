<?php
/**
 * Created by PhpStorm.
 * User: Pierre van Horick
 * Date: 2017/08/20
 * Time: 8:49 PM
 */

namespace App\Interfaces;


interface CardInterface
{
    public function getType($cardNumber);
}