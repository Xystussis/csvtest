<?php
/**
 * Created by PhpStorm.
 * User: Pierre van Horick
 * Date: 2017/08/20
 * Time: 7:11 PM
 */

namespace App\Interfaces;


interface CSVInterface
{
    public function appendToCSV($csv,\JsonSerializable $object);
    public function writeArrayToCsv($csv,array $array);
    public function writeToCsv($csv, \JsonSerializable $object);

}