<?php
/**
 * Created by PhpStorm.
 * User: Pierre van Horick
 * Date: 2017/08/20
 * Time: 7:00 PM
 */

namespace App\Interfaces;


interface ParserInterface
{
    public function parseFile($filename);
}