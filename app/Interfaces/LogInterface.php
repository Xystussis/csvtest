<?php
/**
 * Created by PhpStorm.
 * User: Pierre van Horick
 * Date: 2017/08/20
 * Time: 7:11 PM
 */

namespace App\Interfaces;


interface LogInterface
{
    public function info($line);
}