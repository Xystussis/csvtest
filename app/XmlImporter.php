<?php
/**
 * Created by PhpStorm.
 * User: Pierre van Horick
 * Date: 2017/08/20
 * Time: 7:03 PM
 */

namespace App;


use App\Interfaces\ParserInterface;
use Mockery\Exception;

class XmlImporter implements ParserInterface
{
    protected $blockString;
    protected $object = null;
    protected $identifier = "";
    protected $csvWriter = null;
    protected $log = null;


    public function __construct()
    {
        $this->csvWriter = new CSVWriter();
        $this->csvWriter->writeArrayToCsv('storage/people.csv',["name","email","phone","dob","credit card type","interests space seperated"]);
        $this->log = new Log();

    }


    public function parseFile($filename)
    {
        try
        {
            $handle = fopen($filename, "r");

            if (!$handle)
            {
                return false;
            }

            $i = 0;
            $parser = xml_parser_create();
            xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, false);

            xml_set_element_handler($parser, array(&$this, "start"), array(&$this, "stop"));
            xml_set_character_data_handler($parser, array(&$this, "char"));
            //xml_parse($parser,)

            while (!feof($handle))
            {
                xml_parse($parser, fread($handle, 4096));
                $i++;
            }
            xml_parse($parser, "", true);
            fclose($handle);
            xml_parser_free($parser);
        }
        catch (Exception $e)
        {
            return false;
        }
        return true;
    }


    private function start(&$parser, $name, $attribs)
    {


        if ($name == "category")
        {
            //We don't want to make 10 00000 new categories.
            $this->object = \App\Category::firstOrNew(['legacy_id'=>$attribs['id']]);
            $this->object->legacy_id = $attribs['id'];
        }
        else if ($name == "person")
        {
            $this->object = \App\Person::firstOrNew(['legacy_id'=>$attribs['id']]);
            $this->object->legacy_id = $attribs['id'];
        }
        else
        {
            if (method_exists($this->object,"set".ucfirst($name)))
            {
                $this->identifier = $name;
            }
            elseif (method_exists($this->object,"add".ucfirst($name)))
            {
                $this->identifier = $name;
            }

        }

        /**
         * @TODO refactor this.
         */
        if ($this->object)
        {
            foreach ($attribs as $key => $val)
            {
                if ($key != "id")
                {
                    if (method_exists($this->object, "set" . ucfirst($key)))
                    {
                        $this->object->{"set" . ucfirst($key)}($val);
                    }
                    elseif (method_exists($this->object, "add" . ucfirst($key)))
                    {
                        $this->object->{"add" . ucfirst($key)}($val);
                    }
                }
            }
        }
//        $this->info(print_r($attribs,true));
    }

    private function stop(&$parser, $name)
    {

        if ($name == "category")
        {
            if ($this->object)
            {
                $this->object->save();
                $this->object = null;
            }

        }
        else if ($name == "person")
        {
            if ($this->object)
            {
//                $this->object->save();
                $this->csvWriter->appendToCSV("storage/people.csv",$this->object);
                $this->log->info('Writing '.$this->object->name);

                $this->object = null;
            }
        }
        if ($name == $this->identifier)
        {
            $this->identifier = "";
        }


//        $this->error("Ending: $name");
    }

    private function char (&$parser,$string)
    {
        if ($this->object)
        {
            //To set values
            if (method_exists($this->object,"set".ucfirst($this->identifier)))
            {
                if (!empty(trim($string)))
                {
                    $this->object->{"set" . ucfirst($this->identifier)}($string);

//                    $this->identifier = "";
                }
            }
            //Add multiple values
            else if (method_exists($this->object,"add".ucfirst($this->identifier)))
            {
                if (!empty(trim($string)))
                {
                    $this->object->{"add" . ucfirst($this->identifier)}($string);

//                    $this->identifier = "";
                }
            }

        }
//        $this->info("Data: ".$string);
    }


}