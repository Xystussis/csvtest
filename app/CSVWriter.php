<?php
/**
 * Created by PhpStorm.
 * User: Pierre van Horick
 * Date: 2017/08/20
 * Time: 7:55 PM
 */

namespace App;


use App\Interfaces\CSVInterface;

class CSVWriter implements CSVInterface
{

    public function writeArrayToCsv($csv,array $array)
    {

        $fileHandle = fopen($csv, 'w');
        fputcsv($fileHandle, $array);
        fclose($fileHandle);
        return true;
    }


    public function writeToCsv($csv, \JsonSerializable $object)
    {

        if (!file_exists($csv))
        {
            return false;
        }
        $fileHandle = fopen($csv, 'w');
        fputcsv($fileHandle, json_decode(json_encode($object),true));
        fclose($fileHandle);
        return true;

    }


    public function appendToCSV($csv, \JsonSerializable $object)
    {

        if (!file_exists($csv))
        {
            return false;
        }

        $fileHandle = fopen($csv, 'a');
        fputcsv($fileHandle, json_decode(json_encode($object),true));
        fclose($fileHandle);
        return true;
    }
}