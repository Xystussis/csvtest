<?php
/**
 * Created by PhpStorm.
 * User: Pierre van Horick
 * Date: 2017/08/20
 * Time: 8:50 PM
 */

namespace App;


use App\Interfaces\CardInterface;

class CreditCard implements CardInterface
{

    protected $types =
        [


            "Diners Club Carte Blanche" => [[300,305]],
            "Diners Club International" => [[300,305], 309, 36, [38,39]],
            "Discover Card" => [6011, [622126,622925], [644,649], 65],
            "JCB" => [[3528,3589]],
            "Laser" => [6304, 6706, 6771, 6709],
            "Maestro" => [5018, 5020, 5038, 5612, 5893, 6304, 6759, 6761, 6762, 6763, 0604, 6390],
            "Dankort" => [5019],
            "Visa Electron" => [4026, 417500, 4405, 4508, 4844, 4913, 4917],
            "China UnionPay" => [62, 88],
            "Diners Club US & Canada" => [54, 55],
            "MasterCard" => [[50,55]],
            "American Express" => [34, 37],
            "Visa" => [4],
            "Japan Credit Beau" => [99]

        ];


    public function getType($cardNumber)
    {
        $cardNumber = trim($cardNumber);
        foreach ($this->types as $key=>$set)
        {

            foreach ($set as $ind=>$val)
            {
                if (is_array($val))
                {

                    $start = $val[0];
                    $end = $val[1];

                    $number = substr($cardNumber, 0, strlen($start));
//                print($number).PHP_EOL;
                    if ($number >= $start && $number <= $end && !empty($number))
                    {
                        return $key;
                    }
                }
                else
                {
                    $number = substr($cardNumber, 0, strlen($val));
                    if ($number == $val && !empty($number))
                    {
                        return $key;
                    }
                }
            }
        }
        return false;
    }
}