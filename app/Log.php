<?php
/**
 * Created by PhpStorm.
 * User: Pierre van Horick
 * Date: 2017/08/20
 * Time: 8:41 PM
 */

namespace App;


use App\Interfaces\LogInterface;
use Carbon\Carbon;

class Log implements LogInterface
{

    public function info($line)
    {
        $fileHandle = fopen('storage/logs/info.log', 'a');
        fwrite($fileHandle, Carbon::now()." ".$line.PHP_EOL);
        fclose($fileHandle);
    }
}